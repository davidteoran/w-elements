import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import './App.css';

import Dash from './components/dash/dash';

class Welements extends React.Component {
	render() {
	  	return (
			<Switch>
				<Route exact path="/" render={() => (<Redirect to="/dash/weather" />)} />
				<Route exact path="/dash" render={() => (<Redirect to="/dash/weather" />)} />
				<Route exact path="/dash/weather" component={Dash} />
				<Route exact path="/dash/uv-index" component={Dash} />
				<Route exact path="/dash/maps" component={Dash} />
				<Route exact path="/dash/about" component={Dash} />				
				<Route path="*" render={() => (<Redirect to="/" />)} />
			</Switch>
	  	)
	}
}

export default Welements;

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
	AppBar,
	CircularProgress,
	TextField,
	Toolbar,
	Typography,
	IconButton,
	Menu,
	Badge,
	Button,
	withStyles
} from '@material-ui/core';
import { ImmortalDB } from 'immortal-db';
import { isArray, find, forEach, isEmpty } from 'lodash';
import { alpha } from '@material-ui/core/styles/colorManipulator';
import PlacesMenuItem from './placesMenuItem/placesMenuItem';
import PlaceIcon from '@material-ui/icons/Place';
import ClearIcon from '@material-ui/icons/Clear';
import Autocomplete from '@material-ui/lab/Autocomplete';

const styles = theme => ({
	root: {
    	width: '100%'
	},
  	grow: {
    	flexGrow: 1
	},
	iconButtonRoot: {
		padding: '5px'
	},
	button: {
		margin: theme.spacing(1),
		display: 'none',
    	[theme.breakpoints.up('md')]: {
      		display: 'flex'
    	}
  	},
  	title: {
    	display: 'none',
    	[theme.breakpoints.up('sm')]: {
      		display: 'block'
		},
		marginRight: theme.spacing(2)
	},
	search: {
    	position: 'relative',
    	borderRadius: theme.shape.borderRadius,
    	backgroundColor: alpha(theme.palette.common.white, 0.15),
    	'&:hover': {
      		backgroundColor: alpha(theme.palette.common.white, 0.25)
    	},
    	marginRight: theme.spacing(1),
    	marginLeft: theme.spacing(1),
		width: '100%',
    	[theme.breakpoints.up('sm')]: {
      		marginLeft: theme.spacing(3),
      		width: 'auto'
		}
  },
  searchIcon: {
    width: theme.spacing(6),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
	minWidth: '250px !important',
  },
  inputInput: {
    paddingTop: theme.spacing(1),
    paddingRight: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    paddingLeft: theme.spacing(6),
		transition: theme.transitions.create('width'),
		width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 200,
      '&:focus': {
        width: 300,
      }
    }
	}
});

class TopNav extends React.Component {
	state = {
		open: false,
		loading: false,
		anchorEl: null,
		fetchedCities: [],
		foundCities: [],
		selectedCity: {},
		citySearchInputField: ""
	};

	componentDidMount() {
		this._getCitiesFromLS(this.props);
	}

	handlePlacesMenuOpen = event => {
    	this.setState({ anchorEl: event.currentTarget });
	};
	
	handleMenuClose = () => {
    	this.setState({ anchorEl: null });
	};

	handleInputChange = event => {
		if (event && event.target.value) {
			this.setState({ citySearchInputField : event.target.value });
			this.fetchCities(event.target.value);
		}
	}

	async selectCity(city, props) {
		let citiesList = JSON.parse(await ImmortalDB.get('citiesList'));

		forEach(citiesList, listItem => {
			if (listItem.cityId !== city.cityId) {
				listItem.selected = false;
			} else if (listItem.cityId === city.cityId) {
				listItem.selected = true;
			}
		});

		await ImmortalDB.set('citiesList', JSON.stringify(citiesList));
		this.setState({ selectedCity: city }, () => {
			this._getCitiesFromLS(this.props);
		});
		props.setSelectedCity(city);
	}

	async fetchCities(inputValue) {
		let cities;
		this.setState({ loading: true });
		try {
			let response = await fetch('/location/cities', {
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					cityName: inputValue
				})
			});
			let json = await response.json();
			cities = JSON.parse(json);
			this.setState({ loading: false, foundCities: cities.slice(0, 20) });
		}
		catch(e) {
			this.setState({ loading: false, foundCities: [] });
		}
	}

	async _addCityToLS(props) {
		let timezoneJson;
		try {
			let response = await fetch('/location/timezone', {
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					latlng: {
						latitude: this.state.selectedCity.loc.coordinates[1],
						longitude: this.state.selectedCity.loc.coordinates[0]
					}
				})
			});
			let json = await response.json();
			timezoneJson = JSON.parse(json);
		}
		catch(e) {
			timezoneJson = [];
		}
		const newCity = {
			cityId: this.state.selectedCity.cityId,
			country: this.state.selectedCity.country,
			loc: this.state.selectedCity.loc,
			name: this.state.selectedCity.name,
			timezone: isArray(timezoneJson) && timezoneJson.length > 0 ? timezoneJson : [],
			selected: true
		};
				
		let citiesList = JSON.parse(await ImmortalDB.get('citiesList'));

		if (citiesList === null) {
			citiesList = [];
		}
		if (isArray(citiesList) && !find(citiesList, city => city.cityId === newCity.cityId)) {
			forEach(citiesList, listItem => {
				listItem.selected = false;
			});
			citiesList.push(newCity);
		}

		await ImmortalDB.set('citiesList', JSON.stringify(citiesList));
	
		this.setState({ selectedCity: newCity }, () => {
			props.setSelectedCity(newCity);     
			this._getCitiesFromLS(props);
		});
	}
	
	async _getCitiesFromLS(props) {
		let citiesList = JSON.parse(await ImmortalDB.get('citiesList'));
		if (!citiesList || (isArray(citiesList) && citiesList.length === 0)) {
			props.setSelectedCity({});
			this.setState({ fetchedCities: [] });
		} else {
			if (!find(citiesList, listItem => listItem.selected)) {
				citiesList[0].selected = true;
				await ImmortalDB.set('citiesList', JSON.stringify(citiesList));
			}
			this.setState({ fetchedCities: citiesList, foundCities: citiesList.filter(city => city.selected) }, () => {
				if (this.state.fetchedCities.length > 0) {
					let selectedCity = find(this.state.fetchedCities, city => city.selected);
					this.setState({
						selectedCity: selectedCity,
						citySearchInputField: selectedCity.name + ", " + selectedCity.country
					});
					props.setSelectedCity(selectedCity);
				}
			});
		}
	}

	async removeCityFromLS(city, props) {
		let citiesList = JSON.parse(await ImmortalDB.get('citiesList'));
		citiesList = citiesList.filter(cityFromList => cityFromList.cityId !== city.cityId);
		if (city.selected && citiesList.length > 0) {
			citiesList[0].selected = true;
		}
		await ImmortalDB.set('citiesList', JSON.stringify(citiesList));
		this.setState({ selectedCity: {} }, () => this._getCitiesFromLS(props));
	}

	_handleEnterPress = event => {
		event.preventDefault();
		if (!isEmpty(this.state.foundPlace)) {
			this._addCityToLS(this.props);
		}
	}

	render() {
		const { classes } = this.props;
		const { anchorEl } = this.state;
		const isMenuOpen = Boolean(anchorEl);

		const cities = this.state.fetchedCities.map((city, index) => {
			return (
				<PlacesMenuItem
					key={index}
					city={city}
					selectCity={() => this.selectCity(city, this.props)}
					removeCityFromLS={() => this.removeCityFromLS(city, this.props)}
					handleMenuClose={this.handleMenuClose}
				/>
			);
		});

		const renderPlacesMenu = (
			<Menu
				anchorEl={anchorEl}
				anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
				transformOrigin={{ vertical: 'top', horizontal: 'right' }}
				open={isMenuOpen}
				onClose={this.handleMenuClose}
			>
        		{cities}
      		</Menu>
    	);

		return (
			<div className={classes.root}>
				<AppBar position="fixed" color="primary">
				<Toolbar variant="dense">
					<Typography variant="h6" color="initial" className={classes.title}>
						W-Elements
					</Typography>
					<Button
						component={Link}
						to="/dash/weather"
						className={classes.button}
						color={this.props.location.pathname === "/dash/weather" ? "secondary" : "default"}
					>
						Weather
					</Button>
					<Button
						component={Link}
						to="/dash/uv-index"
						className={classes.button}
						color={this.props.location.pathname === "/dash/uv-index" ? "secondary" : "default"}
					>
						UV
					</Button>
					<Button
						component={Link}
						to="/dash/maps"
						className={classes.button}
						color={this.props.location.pathname === "/dash/maps" ? "secondary" : "default"}
					>
						Maps
					</Button>
					<Button
						component={Link}
						to="/dash/about"
						className={classes.button}
						color={this.props.location.pathname === "/dash/about" ? "secondary" : "default"}
					>
						About
					</Button>
					<div className={classes.grow} />
					<div className={classes.search}>
						<div className={classes.searchIcon}>
							<PlaceIcon />				
						</div>
						<form
							className={classes.container}
							noValidate
							autoComplete="off"
							onSubmit={this._handleEnterPress}
						>
							<Autocomplete
								id="city-search"
								classes={{
									root: classes.inputRoot,
									input: classes.inputInput,
								}}
								open={this.state.open}
								onOpen={() => this.setState({ open: true })}
								onClose={() => this.setState({ open: false })}
								getOptionSelected={(option, value) => value.cityId && option.cityId === value.cityId}
								getOptionLabel={(option) => option.name + ', ' + option.country}
								options={this.state.foundCities}
								loading={this.state.loading}
								popupIcon={null}
								value={this.state.selectedCity}
								inputValue={this.state.citySearchInputField}
								onInputChange={this.handleInputChange}
								onChange={(event, value) => {
									if (value) {
										this.setState({ selectedCity: value }, () => {
											this._addCityToLS(this.props);
										});
									} else {
										this.setState({ citySearchInputField : event.target.value });
									}
								}}
								renderInput={(params) => (
									<TextField
										{...params}
										placeholder="Search for a city…"
										variant="outlined"
										size='small'
										InputProps={{
											...params.InputProps,
											style: { paddingRight: '8px', paddingLeft: '36px' },
											endAdornment: (
												this.state.loading ?
													<React.Fragment>
														{this.state.loading ? <CircularProgress color="inherit" size={20} /> : null}
														{params.InputProps.endAdornment}
													</React.Fragment> :
														this.state.fetchedCities.length > 0 ?
															<IconButton
																classes={{ root: classes.iconButtonRoot }}
																onClick={() => this.setState({ citySearchInputField: "" })}
															>
																<ClearIcon />
															</IconButton> :
															null
											),
										}}
									/>
								)}
							/>
						</form>
					</div>
					<IconButton color="inherit" onClick={this.handlePlacesMenuOpen}>
						<Badge badgeContent={this.state.fetchedCities.length} color="secondary" overlap='rectangular'>
							<PlaceIcon />
						</Badge>
					</IconButton>            
				</Toolbar>
				</AppBar>
				{renderPlacesMenu}
			</div>
		);
	}
}

TopNav.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(TopNav);
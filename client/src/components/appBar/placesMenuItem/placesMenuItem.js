import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { MenuItem, ListItemSecondaryAction, IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';

const styles = theme => ({
	menuItemRoot: {
		paddingRight: theme.spacing(7)
	}
});

class PlacesMenuItem extends React.Component {
    
  shouldComponentUpdate(nextProps, nextState) {
    if ((this.props.city.selected !== nextProps.city.selected) || (this.props.city.cityId !== nextProps.city.cityId)) {
      return true;
    }
    return false;
  }

	render() {
		const { classes } = this.props;

		return (
			<MenuItem
				key={this.props.key}
				onClick={() => { this.props.selectCity(); this.props.handleMenuClose(); }}
				onKeyDown={this.props.drawerToggle}
				selected={this.props.city.selected}
				classes={{ root: classes.menuItemRoot }}
			>
				{this.props.city.name}
				<ListItemSecondaryAction>
          <IconButton edge="end" aria-label="Delete" onClick={() => { this.props.removeCityFromLS(); this.props.handleMenuClose(); }}>
            <DeleteIcon />
          </IconButton>
        </ListItemSecondaryAction>
			</MenuItem>      
    );
	}
}

PlacesMenuItem.propTypes = {
	classes: PropTypes.object.isRequired
};
  
export default withStyles(styles)(PlacesMenuItem);
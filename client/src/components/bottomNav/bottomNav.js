import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Hidden, BottomNavigation, BottomNavigationAction, withStyles } from '@material-ui/core';
import TodayIcon from '@material-ui/icons/Today';
import WbSunnyIcon from '@material-ui/icons/WbSunny';
import MapIcon from '@material-ui/icons/Map';

const styles = theme => ({
	bottomNavRoot: {
		width: '100%',
		position: 'fixed',
		bottom: 0,
		left: 0
	},
	bottomNavActionRoot: {
		color: theme.palette.text.primary
	},
	bottomNavActionSelected: {
		color: theme.palette.secondary.main
	}
});

class BottomNav extends React.Component {
	state = {
		value: this.props.location.pathname
	};
    
	changePath = (event, value) => {
		event.preventDefault();
		this.props.history.push(value);
		window.scrollTo(0, 0);
		this.setState({ value });
	};

	static getDerivedStateFromProps(nextProps, prevState) {
		if (nextProps.value !== prevState.value) {
			return {
				value: nextProps.location.pathname
			};
		}

		return null;
	}
    
	render() {
		const { classes } = this.props;
		const { value } = this.state;
    
		return (
			<Hidden mdUp>
				<BottomNavigation value={value} onChange={this.changePath} classes={{ root: classes.bottomNavRoot }} showLabels>
					<BottomNavigationAction label="Weather" value="/dash/weather" icon={<TodayIcon color={this.state.value === "/dash/weather" ? "secondary" : "inherit"} />} classes={{ root: classes.bottomNavActionRoot, selected: classes.bottomNavActionSelected }} />
					<BottomNavigationAction label="UV" value="/dash/uv-index" icon={<WbSunnyIcon color={this.state.value === "/dash/uv-index" ? "secondary" : "inherit"} />} classes={{ root: classes.bottomNavActionRoot, selected: classes.bottomNavActionSelected }} />
					<BottomNavigationAction label="Maps" value="/dash/maps" icon={<MapIcon color={this.state.value === "/dash/maps" ? "secondary" : "inherit"} />} classes={{ root: classes.bottomNavActionRoot, selected: classes.bottomNavActionSelected }} />
				</BottomNavigation>
			</Hidden>
		);
	}
}

BottomNav.propTypes = {
	classes: PropTypes.object.isRequired
};
  
export default withRouter(withStyles(styles)(BottomNav));
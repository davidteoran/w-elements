import React from 'react';
import PropTypes from 'prop-types';
import {
	Grid,
	Accordion,
	AccordionSummary,
	AccordionDetails,
	Typography,
	Button,
	Checkbox,
	Paper,
	withStyles,
	Hidden,
	List,
	ListItem,
	ListItemAvatar,
	ListItemText,
	Avatar
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { ImmortalDB } from 'immortal-db';
import ErrorIcon from '@material-ui/icons/Error';
import Moment from 'moment';
import LoadScreen from '../../loadScreen/loadScreen';

const styles = theme => ({
	paper: {
		paddingTop: 16,
		paddingBottom: 16
	},
	paperWarning: {
		textAlign: 'center',
		paddingTop: 16,
		paddingBottom: 16,
		marginTop: theme.spacing(3)
	},
	errorIcon: {
		fontSize: '3.5em',
		color: '#F44336'
	},
	textCenter: {
		textAlign: 'center'
	},
	textRight: {
		textAlign: 'right'
	},
	lowUVCat: {
		backgroundColor: '#558B2F'
	},
	modUVCat: {
		backgroundColor: '#F9A825'
	},
	highUVCat: {
		backgroundColor: '#EF6C00'
	},
	vHighUVCat: {
		backgroundColor: '#B71C1C'
	},
	extrUVCat: {
		backgroundColor: '#6A1B9A'
	},
	lowUVText: {
		color: '#558B2F'
	},
	modUVText: {
		color: '#F9A825'
	},
	highUVText: {
		color: '#EF6C00'
	},
	vHighUVText: {
		color: '#B71C1C'
	},
	extrUVText: {
		color: '#6A1B9A'
	},
	skinType1: {
		display: 'inline-block', 
		marginTop: '-1px',
		marginLeft: '10px',
		borderRadius: '50%',
		width: '15px',
		height: '15px',
		background: '#F1D1B1'
	},
	skinType2: {
		display: 'inline-block', 
		marginTop: '-1px',
		marginLeft: '10px',
		borderRadius: '50%',
		width: '15px',
		height: '15px',
		background: '#E4B590'
	},
	skinType3: {
		display: 'inline-block', 
		marginTop: '-1px',
		marginLeft: '10px',
		borderRadius: '50%',
		width: '15px',
		height: '15px',
		background: '#CF9F7D'
	},
	skinType4: {
		display: 'inline-block', 
		marginTop: '-1px',
		marginLeft: '10px',
		borderRadius: '50%',
		width: '15px',
		height: '15px',
		background: '#B67851',
		color: '#ffffff'
	},
	skinType5: {
		display: 'inline-block',
		marginTop: '-1px',
		marginLeft: '10px',
		borderRadius: '50%',
		width: '15px',
		height: '15px',
		background: '#A15E2D',
		color: '#ffffff'
	},
	skinType6: {
		display: 'inline-block',
		marginTop: '-1px',
		marginLeft: '10px',
		borderRadius: '50%',
		width: '15px',
		height: '15px',
		background: '#513938',
		color: '#ffffff'
	}
});

class UVIndex extends React.Component {
	state = {
		isLoading: true,
		fetchError: false,
		selectedCity: {},       
		uvIndexData: {},
		selectedSkinType: ""
	};
    
	static getDerivedStateFromProps(nextProps, prevState) {
		if (nextProps.selectedCity !== prevState.selectedCity) {
			return {
				selectedCity: nextProps.selectedCity
			};
		}

		return null;
	}

	componentDidMount() {
		this._getUVIndexData();
		this._getSkinTypeFromLS();
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevState.selectedCity !== this.state.selectedCity) {
			this._getUVIndexData();
			this._getSkinTypeFromLS();        
		}
	}

	render(props) {
		const { classes } = this.props;

		return (
			this.state.isLoading && !this.state.fetchError ? 
			<LoadScreen /> :
			!this.state.isLoading && this.state.fetchError ?
			<Paper className={classes.paperWarning} elevation={1}>
				<ErrorIcon className={classes.errorIcon} />
				<Typography variant="subtitle1" paragraph>
					An error occured while fetching the data. Please try again later.
				</Typography>
			</Paper> :
			<Grid container spacing={2}>
				<Grid item xs={12} lg={6} className={classes.textCenter}>
					<Paper className={classes.paper} elevation={1}>
						<Typography variant="h5" component="h3">
							Max UV index today: <span className={classes[this._mapUVValueToCat(Number(this.state.uvIndexData.uv_max).toFixed(1))]}>{Number(this.state.uvIndexData.uv_max).toFixed(1)}</span>
						</Typography>
					</Paper>
				</Grid>
				<Grid item xs={12} lg={6} className={classes.textCenter}>
					<Paper className={classes.paper} elevation={1}>
						<Typography variant="h5" component="h3">
							Current UV index: <span className={classes[this._mapUVValueToCat(Number(this.state.uvIndexData.uv).toFixed(1))]}>{Number(this.state.uvIndexData.uv).toFixed(1)}</span>
						</Typography>
					</Paper>
				</Grid>
				<Grid item xs={12}>
					<Paper className={classes.paper} elevation={1}>
						<Grid container>
							<Hidden smDown>
								<Grid item lg={4}>
									<Button color="default" onClick={() => this._selectSkinType("st1")}>
										<Checkbox color="default" checked={this.state.selectedSkinType === "st1"} />
										Skin type I
										<div className={classes.skinType1}></div>
									</Button>
								</Grid>
								<Grid item lg={4} className={classes.textCenter}>
									<Button color="default" onClick={() => this._selectSkinType("st2")}>
										<Checkbox color="default" checked={this.state.selectedSkinType === "st2"} />
										Skin type II
										<div className={classes.skinType2}></div>
									</Button>
								</Grid>
								<Grid item lg={4} className={classes.textRight}>
									<Button color="default" onClick={() => this._selectSkinType("st3")}>
										<Checkbox color="default" checked={this.state.selectedSkinType === "st3"} />
										Skin type III
										<div className={classes.skinType3}></div>
									</Button>
								</Grid>
								<Grid item lg={4}>
									<Button color="default" onClick={() => this._selectSkinType("st4")}>
										<Checkbox color="default" checked={this.state.selectedSkinType === "st4"} />
											Skin type IV
											<div className={classes.skinType4}></div>
									</Button>
								</Grid>
								<Grid item lg={4} className={classes.textCenter}>
									<Button color="default" onClick={() => this._selectSkinType("st5")}>
										<Checkbox color="default" checked={this.state.selectedSkinType === "st5"} />
										Skin type V
										<div className={classes.skinType5}></div>
									</Button>
								</Grid>
								<Grid item lg={4} className={classes.textRight}>
									<Button color="default" onClick={() => this._selectSkinType("st6")}>
										<Checkbox color="default" checked={this.state.selectedSkinType === "st6"} />
										Skin type VI
										<div className={classes.skinType6}></div>
									</Button>
								</Grid>
								{this.state.selectedSkinType === "" ?
								<Grid item lg={12} className={classes.textCenter}>
									<Typography variant="subtitle1">
										Please select your skin type to view how much time you can spend on direct sunlight safely!
									</Typography>
								</Grid>:
								<Grid item lg={12} className={classes.textCenter}>
									<Typography variant="subtitle1">
										Current safe sun exposure time for your skin type: <b>{this._calculateSafeExposureTime(this.state.uvIndexData.safe_exposure_time)}</b>
									</Typography>
								</Grid>}
							</Hidden>
							<Hidden smUp>
								<div className={classes.textCenter}>
									<Grid item xs={12}>
										<Button color="default" onClick={() => this._selectSkinType("st1")}>
											<Checkbox color="default" checked={this.state.selectedSkinType === "st1"} />
											Skin type I
											<div className={classes.skinType1}></div>
										</Button>
										<Button color="default" onClick={() => this._selectSkinType("st2")}>
											<Checkbox color="default" checked={this.state.selectedSkinType === "st2"} />
											Skin type II
											<div className={classes.skinType2}></div>
										</Button>
										<Button color="default" onClick={() => this._selectSkinType("st3")}>
											<Checkbox color="default" checked={this.state.selectedSkinType === "st3"} />
											Skin type III
											<div className={classes.skinType3}></div>
										</Button>
										<Button color="default" onClick={() => this._selectSkinType("st4")}>
											<Checkbox color="default" checked={this.state.selectedSkinType === "st4"} />
											Skin type IV
											<div className={classes.skinType4}></div>
										</Button>
										<Button color="default" onClick={() => this._selectSkinType("st5")}>
											<Checkbox color="default" checked={this.state.selectedSkinType === "st5"} />
											Skin type V
											<div className={classes.skinType5}></div>
										</Button>
										<Button color="default" onClick={() => this._selectSkinType("st6")}>
											<Checkbox color="default" checked={this.state.selectedSkinType === "st6"} />
											Skin type VI
											<div className={classes.skinType6}></div>
										</Button>
									</Grid>
									{this.state.selectedSkinType === "" ?
									<Grid item xs={12}>
										<Typography variant="subtitle1">
											Please select your skin type to view how much time you can spend on direct sunlight safely!
										</Typography>
									</Grid>:
									<Grid item xs={12}>
										<Typography variant="subtitle1">
											Current safe sun exposure time for your skin type: <b>{this._calculateSafeExposureTime(this.state.uvIndexData.safe_exposure_time)}</b>
										</Typography>
									</Grid>}
								</div>
							</Hidden>
						</Grid>
					</Paper>
				</Grid>
				<Grid item xs={12}>
					<Accordion>
						<AccordionSummary expandIcon={<ExpandMoreIcon />}>
							<Typography variant="subtitle1" color="textSecondary">UV INDEX SCALE</Typography>
						</AccordionSummary>
						<AccordionDetails>
							<Grid item xs={12}>
								<List>
									<ListItem divider>
										<ListItemAvatar>
											<Avatar className={classes.extrUVCat}>11+</Avatar>
										</ListItemAvatar>
										<ListItemText>
											<b className={classes.extrUVText}>Extreme</b> - Reschedule outdoor activities for early morning and evening, full protection essential
										</ListItemText>
									</ListItem>
									<ListItem divider>
										<ListItemAvatar>
											<Avatar className={classes.vHighUVCat}>8-11</Avatar>
										</ListItemAvatar>
										<ListItemText>
											<b className={classes.vHighUVText}>Very high</b> - Shade needed, advised to stay indoors if possible
										</ListItemText>
									</ListItem>
									<ListItem divider>
										<ListItemAvatar>
											<Avatar className={classes.highUVCat}>6-8</Avatar>
										</ListItemAvatar>
										<ListItemText>
											<b className={classes.highUVText}>High</b> - Protect body with clothing and sunscreen, avoid long exposure to sun
										</ListItemText>
									</ListItem>
									<ListItem divider>
										<ListItemAvatar>
											<Avatar className={classes.modUVCat}>3-6</Avatar>
										</ListItemAvatar>
										<ListItemText>
											<b className={classes.modUVText}>Moderate</b> - Protection required, seek shade during midday hours
										</ListItemText>
									</ListItem>
									<ListItem>
										<ListItemAvatar>
											<Avatar className={classes.lowUVCat}>0-3</Avatar>
										</ListItemAvatar>
										<ListItemText>
											<b className={classes.lowUVText}>Low</b> - No protection required
										</ListItemText>
									</ListItem>
								</List>
							</Grid>
						</AccordionDetails>
					</Accordion>
				</Grid>
			</Grid>
		);
	}
			
	async _getUVIndexData() {
		try {
			if (Object.keys(this.state.selectedCity).length > 0) {
				let response = await fetch('/weather/uv-index', {
					method: 'POST',
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						latlng: {
							latitude: this.state.selectedCity.loc.coordinates[1],
							longitude: this.state.selectedCity.loc.coordinates[0]
						}
					})
				});
				let json = await response.json();
				this.setState({ uvIndexData: json.result, isLoading: false });
			}
		}
		catch(e) {
			this.setState({ isLoading: false, fetchError: true });
		}
	}

	_mapUVValueToCat = uvIndex => {
		if (uvIndex < 3) {
			return "lowUVText";
		} else if (uvIndex >= 3 && uvIndex < 6) {
			return "modUVText";
		} else if (uvIndex >= 6 && uvIndex < 8) {
			return "highUVText";
		} else if (uvIndex >= 8 && uvIndex < 11) {
			return "vHighUVText";
		} else if (uvIndex >= 11) {
			return "extrUVText";
		} else {
			return "";
		}
	}

	async _selectSkinType(skinType) {
		await ImmortalDB.set('skinType', skinType);
		this.setState({ selectedSkinType: skinType });
	}

	async _getSkinTypeFromLS() {
		const skinType = await ImmortalDB.get('skinType');
		if (skinType && skinType !== '') {
			this.setState({ selectedSkinType: skinType });
		}
	}

	_calculateSafeExposureTime = safeExposureTimes => {
		if (safeExposureTimes[this.state.selectedSkinType] === null) {
			return '-';
		} else if (safeExposureTimes[this.state.selectedSkinType] > 0) {
			return Moment.duration(safeExposureTimes[this.state.selectedSkinType], 'minutes').humanize();
		}
	}
}

UVIndex.propTypes = {
	classes: PropTypes.object.isRequired
};
  
export default withStyles(styles)(UVIndex);
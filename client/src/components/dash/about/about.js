import React from 'react';
import PropTypes from 'prop-types';
import {
	Grid,
	Card,
	CardHeader,
	CardContent,
	List,
	ListItem,
	ListItemAvatar,
	ListItemText,
	Avatar,
	withStyles
} from '@material-ui/core';
import OpenWeatherMap from '../../../images/open-weather-map-logo.svg';
import OpenUVLogo from '../../../images/open-uv-logo.png';
import ReactLogo from '../../../images/react-logo.svg';
import MaterialUiLogo from '../../../images/material-ui-logo.svg';
import WeatherIconsLogo from '../../../images/wi-day-cloudy.svg';

const styles = theme => ({
	spacing: {
		'@media (max-width: 599px)': {
			padding: '0 10px'
		},
		'@media (min-width: 600px)': {
			padding: '0 18px'
		}
	},
	card: {
		marginBottom: theme.spacing(2)
	}
});

class About extends React.Component {

	render() {
		const { classes } = this.props;

		return (
			<Grid container>
				<Grid item xs={12} lg={6} className={classes.spacing}>
					<Card className={classes.card}>
						<CardHeader title="Data providers"></CardHeader>
						<CardContent>
							<List>
								<ListItem dense button component="a" href="https://openweathermap.org/" target="_blank" rel="noopener noreferrer">
									<ListItemAvatar>
										<Avatar src={OpenWeatherMap} alt="" />
									</ListItemAvatar>									
									<ListItemText>OpenWeatherMap</ListItemText>
								</ListItem>
								<ListItem dense button component="a" href="https://www.openuv.io/" target="_blank" rel="noopener noreferrer">
										<ListItemAvatar>
											<Avatar src={OpenUVLogo} alt="" />
										</ListItemAvatar>
										<ListItemText>OpenUV</ListItemText>
									</ListItem>
								</List>
							</CardContent>
						</Card>
					</Grid>
					<Grid item xs={12} lg={6} className={classes.spacing}>
						<Card>
							<CardHeader title="Resources"></CardHeader>
							<CardContent>
							<List>
								<ListItem dense button component="a" href="https://reactjs.org/" target="_blank" rel="noopener noreferrer">
									<ListItemAvatar>
										<Avatar src={ReactLogo} alt="" />
									</ListItemAvatar>
									<ListItemText>React</ListItemText>
								</ListItem>
								<ListItem dense button component="a" href="https://material-ui.com/" target="_blank" rel="noopener noreferrer">
									<ListItemAvatar>
										<Avatar src={MaterialUiLogo} alt="" />
									</ListItemAvatar>
									<ListItemText>Material-UI</ListItemText>
								</ListItem>
								<ListItem dense button component="a" href="http://erikflowers.github.io/weather-icons/" target="_blank" rel="noopener noreferrer">
									<ListItemAvatar>
										<Avatar src={WeatherIconsLogo} alt="" />
									</ListItemAvatar>
									<ListItemText>Weather icons</ListItemText>
								</ListItem>	
							</List>
						</CardContent>
					</Card>
				</Grid>
			</Grid>
		);
	}
}

About.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(About);
import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import {
	Grid,
	Paper,
	Typography,
	withStyles
} from '@material-ui/core';
import PlaceIcon from '@material-ui/icons/Place';
import LoadScreen from '../loadScreen/loadScreen';
import AppBar from '../appBar/appBar';
import BottomNav from '../bottomNav/bottomNav';
import About from './about/about';
import { isObject } from 'lodash';

const Now = React.lazy(() => import('./now/now'));
const Forecast = React.lazy(() => import('./forecast/forecast'));
const UVIndex = React.lazy(() => import('./UVIndex/UVIndex'));
const Maps = React.lazy(() => import('./maps/maps'));

const styles = theme => ({
	paperWarning: {
		textAlign: 'center',
		paddingTop: 16,
		paddingBottom: 16,
		marginTop: theme.spacing(3)
	},
	addButton: {
		backgroundColor: '#81C784',
		width: '100%',
		'&:hover': {
			backgroundColor: '#4CAF50'
		}
	},
	placeIcon: {
		fontSize: '3.5em',
		color: '#42A5F5'
	},
	content: {
		marginBottom: theme.spacing(10),
		[theme.breakpoints.up('md')]: {
			marginBottom: 0
		}
  },
  toolbar: theme.mixins.toolbar
});


class Dash extends React.Component {
	state = {
		selectedCity: {},
		cityListEmpty: false
	};

	setSelectedCity = city => {
		if (isObject(city) && Object.keys(city).length > 0) {
			this.setState({ selectedCity: city, cityListEmpty: false });
		} else {
			this.setState({ selectedCity: {}, cityListEmpty: true });
		}
	}
	
	render() {
		const { classes } = this.props;

		return (
			<div>
				<AppBar
					location={this.props.location}
					setSelectedCity={this.setSelectedCity}
				/>
				<main className={classes.content}>
					<div className={classes.toolbar} />
					{(() => {
						switch(this.props.location.pathname) {
							case '/dash/weather':
								return this.state.cityListEmpty ?
								<Grid
									container
									justifyContent="center"
									alignItems="center"
								>
									<Grid item xs={12} md={10}>
										<Paper className={classes.paperWarning} elevation={1}>
											<PlaceIcon className={classes.placeIcon} />
											<Typography variant="subtitle1" paragraph>
												Find your city to get the current weather information.
											</Typography>
										</Paper>
									</Grid>
								</Grid> :
								<Grid
									container
									justifyContent="center"
									alignItems="center"
									spacing={2}
								>
									<Suspense fallback={<LoadScreen />}>
										<Grid item xs={12} md={10}>
												<Now selectedCity={this.state.selectedCity} />
										</Grid>
										<Grid item xs={12} md={10}>
												<Forecast selectedCity={this.state.selectedCity} />
										</Grid>
									</Suspense>
								</Grid>;
							case '/dash/uv-index':
								return this.state.cityListEmpty ? 
								<Grid
									container
									justifyContent="center"
									alignItems="center"
								>
									<Grid item xs={12} md={10}>
										<Paper className={classes.paperWarning} elevation={1}>
											<PlaceIcon className={classes.placeIcon} />
											<Typography variant="subtitle1" paragraph>
												Find your city to get the UV index data.
											</Typography>
										</Paper>
									</Grid>
								</Grid> :
								<Grid
									container
									justifyContent="center"
									alignItems="center"
								>
									<Grid item xs={12} md={10}>
										<Suspense fallback={<LoadScreen />}>
											<UVIndex selectedCity={this.state.selectedCity} />
										</Suspense>
									</Grid>
								</Grid>;
							case '/dash/maps':
								return this.state.cityListEmpty ?
								<Grid
									container
									justifyContent="center"
									alignItems="center"
								>
									<Grid item xs={12} md={10}>
										<Paper className={classes.paperWarning} elevation={1}>
											<PlaceIcon className={classes.placeIcon} />
											<Typography variant="subtitle1" paragraph>
												Find your city to show weather data on the map.
											</Typography>
										</Paper>
									</Grid>
								</Grid> :
								<Grid container>
									<Grid item xs={12}>
										<Suspense fallback={<LoadScreen />}>
											<Maps selectedCity={this.state.selectedCity} />
										</Suspense>
									</Grid>
								</Grid>;
							case '/dash/about':
								return (
									<Grid
										container
										justifyContent="center"
										alignItems="center"
									>
										<Grid item xs={12} md={10}>
											<About />
										</Grid>
									</Grid>
								);
							default:
								return <LoadScreen />;
						}
					})()}
				</main>
				<BottomNav location={this.props.location} />
			</div>	
		);
	}
}

Dash.propTypes = {
	classes: PropTypes.object.isRequired
};
  
export default withStyles(styles)(Dash);
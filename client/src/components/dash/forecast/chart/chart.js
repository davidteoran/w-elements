import React from 'react';
import { Line } from 'react-chartjs-2';
import Moment from 'moment';

class Chart extends React.Component {
	render() {
		return (
            <Line data={this._prepareChartData(this.props.chartData)} options={this._prepareChartData(this.props.chartOptions)} height={150}/>
		);
    }

    _prepareChartData = type => {
        const tempData = () => this.props.weatherData.map(period => Math.round(period.main.temp));
        const rainData = () => this.props.weatherData.map(period => period.rain && period.rain['3h']);
        const labels = () => this.props.weatherData.map(period => Moment(period.dt_txt).format("HH:mm"));
        if (type === 'tempData') {
            const tempDataSet = {
                label: 'Temperature',
                data: tempData(),
                fill: false,
                borderColor: 'rgb(249, 46, 57)',
                tension: 0.5
            };
          
            const chartData = {
                labels: labels(),
                datasets: [tempDataSet]
            };
    
            return chartData;
        } else if (type === 'rainData') {
            const rainDataSet = {
                label: 'Precipitation',
                data: rainData(),
                fill: true,
                backgroundColor: 'rgb(66, 141, 210)',
                tension: 0.5
            };
          
            const chartData = {
                labels: labels(),
                datasets: [rainDataSet]
            };
    
            return chartData;
        } else if (type === 'tempOptions') {
            const chartOptions = {
                scales: {
                    y: {
                        ticks: {
                            color: 'rgb(255, 255, 255)', 
                            stepSize: 5,
                            callback: (value) => {
                                return value + ' °';
                            }
                        },
                        beginAtZero: true,
                        suggestedMin: Math.min(...tempData()) - 5,
                        suggestedMax: Math.max(...tempData()) + 5,
                        grid: {
                            drawBorder: false,
                            color: 'rgb(255, 255, 255)',
                            lineWidth: 0.2
                        }
                    },
                    x: {
                        ticks: {
                            color: 'rgb(255, 255, 255)',
                        },
                        grid: {
                            drawBorder: false,
                            display: false
                        }
                    }
                },
                plugins: {
                    legend: {
                        labels: {
                            color: 'rgb(255, 255, 255)'
                        }
                    }
                }
              };
              return chartOptions;
        } else if (type === 'rainOptions') {
            const chartOptions = {
                scales: {
                    y: {
                        ticks: {
                            color: 'rgb(255, 255, 255)',                             
                            stepSize: 1,
                            callback: (value) => {
                                return value + ' mm';
                            }
                        },
                        beginAtZero: true,
                        suggestedMin: 0,
                        suggestedMax: Math.max(...rainData().filter(data => data !== undefined)) + 5,
                        grid: {
                            drawBorder: false,
                            color: 'rgb(255, 255, 255)',
                            lineWidth: 0.2
                        }
                    },
                    x: {
                        ticks: {
                            color: 'rgb(255, 255, 255)',
                        },
                        grid: {
                            drawBorder: false,
                            display: false
                        }
                    }
                },
                plugins: {
                    legend: {
                        labels: {
                            color: 'rgb(255, 255, 255)'
                        }
                    }
                }
              };
              return chartOptions;
        }
    } 
}

export default Chart;
import React from 'react';
import PropTypes from 'prop-types';
import {
	withStyles,
	Paper,
	Typography,
	Accordion,
	AccordionSummary,
	AccordionDetails,
	Grid,
	Avatar,
	Hidden,
	Button,
	ButtonGroup,
	Fade
} from '@material-ui/core';
import { alpha } from '@material-ui/core/styles/colorManipulator';
import ErrorIcon from '@material-ui/icons/Error';
import Moment from 'moment';
import LoadScreen from '../../loadScreen/loadScreen';
import Chart from './chart/chart';
import WeatherIcon from '../../weatherIcon/weatherIcon';
import ExpandMore from '@material-ui/icons/ExpandMore';

const styles = theme => ({
	errorIcon: {
		fontSize: '3.5em',
		color: '#F44336'
	},
	paper: {
		paddingTop: 8,
		paddingBottom: 8
	},
	paperWarning: {
		textAlign: 'center',
		paddingTop: 8,
		paddingBottom: 8,
		marginTop: theme.spacing(2)
	},
	typographyWeekday: {
		flex: 1
	},
	accordionDetailsRoot: {
		display: 'block',
		padding: 0
	},
	accordionSummaryRoot: {
		textTransform: 'uppercase'
	},
	sectionDesktop: {
    marginTop: theme.spacing(1)
	},
	forecastButton: {
		backgroundColor: alpha(theme.palette.primary.main, 0.9)
	}
});

class Forecast extends React.Component {
	state = {
		isLoading: true,
		fetchError: false,
		selectedCity: {},
		days: {},
		weatherData: {},
		chartIndex: null
	};
    
	static getDerivedStateFromProps(nextProps, prevState) {
		if (nextProps.selectedCity !== prevState.selectedCity) {
			return {
				selectedCity: nextProps.selectedCity
			};
		}

		return null;
	}

	componentDidMount() {
		this._getDays();
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevState.selectedCity !== this.state.selectedCity) {
			this._getDays();
		}
	}

	render() {
		const { classes } = this.props;

		const forecastButtons = Object.keys(this.state.days).map((key, index) => {
			return (
				<Button key={index} onClick={() => this._setChartIndex(index)} classes={{ root: this.state.chartIndex === index ? classes.forecastButton : "" }}>
					<Grid
						container
						direction="row"
						justifyContent="center"
						alignItems="center"	
					>
						<Grid item md={12}>
							<Grid 
								container
								direction="column"
								justifyContent="center"
								alignItems="center"
							>
								<Grid item md={6}>
									<Avatar>
										<WeatherIcon
											iconSize="mediumWeatherIcon"
											selectedIconID={key === "dayOne" ? this.state.days[key][key + "Periods"][0].weather[0].id : this.state.days[key][key + "Periods"][3].weather[0].id}
										/>
									</Avatar>
								</Grid>
								<Grid item md={6}>
									{(() => {
										switch(key) {
											case "dayOne":
												return (
													<Typography variant="button" display="inline">
														Today
													</Typography>
												);
											case "dayTwo":
												return (
													<Typography variant="button" display="inline">
														Tomorrow
													</Typography>
												);
											case "dayThree":
												return (
													<Typography variant="button" display="inline">
														{this._mapDayToName(Moment().add(2, 'd').weekday())}
													</Typography>
												);
											case "dayFour":
												return (
													<Typography variant="button" display="inline">
														{this._mapDayToName(Moment().add(3, 'd').weekday())}
													</Typography>
												);
											case "dayFive":
												return (
													<Typography variant="button" display="inline">
														{this._mapDayToName(Moment().add(4, 'd').weekday())}
													</Typography>
												);
											default:
												return null; 
										}
									})()}
								</Grid>
							</Grid>
						</Grid>
						<Grid item md={12}>
							<Typography variant="caption" display="inline">
								{this.state.days[key].maxTemp}
							</Typography>
							<Typography variant="caption" display="inline" color="textSecondary">
								{" | " + this.state.days[key].minTemp + " °C"}
							</Typography>
						</Grid>
					</Grid>
				</Button>);
		});

		const forecastCharts = Object.keys(this.state.days).map((key, index) => {
			return (
				this.state.chartIndex === index ?
				<Fade in={this.state.chartIndex === index} key={index}>
					<Paper elevation={4}>
						<Grid container>
							<Grid item md={6}>
								<Chart weatherData={this.state.days[key][key + "Periods"]} chartData="tempData" chartOptions="tempOptions" />
							</Grid>
							<Grid item md={6}>
								<Chart weatherData={this.state.days[key][key + "Periods"]} chartData="rainData" chartOptions="rainOptions" />
							</Grid>
						</Grid>
					</Paper>
				</Fade>
				: null
			);
		});

		return (
			this.state.isLoading && !this.state.fetchError ? 
			<LoadScreen /> :
			!this.state.isLoading && this.state.fetchError ?
			<Grid
					container
					justifyContent="center"
					alignItems="center"
				>
				<Grid item md={10}>
					<Paper className={classes.paperWarning} elevation={1}>
						<ErrorIcon className={classes.errorIcon} />
						<Typography variant="subtitle1" paragraph>
							An error occured while fetching the data. Please try again later.
						</Typography>
					</Paper>
				</Grid>
			</Grid> :
			<div>
				<Hidden mdUp>
					<Accordion>
						<AccordionSummary expandIcon={<ExpandMore />} classes={{ root: classes.accordionSummaryRoot }}>
							<Typography variant="subtitle1" color="textSecondary" classes={{ root: classes.typographyWeekday }}>
								Today
							</Typography>
							<Typography variant="subtitle1">
								<WeatherIcon iconSize="smallWeatherIcon" selectedIconID={this.state.days.dayOne.dayOnePeriods[0].weather[0].id} />
								{this.state.days.dayOne.maxTemp}° |&nbsp;
							</Typography>
							<Typography variant="subtitle1" color="textSecondary">
								{this.state.days.dayOne.minTemp}°
							</Typography>
						</AccordionSummary>
						<AccordionDetails classes={{ root: classes.accordionDetailsRoot }}>
							<Chart weatherData={this.state.days.dayOne.dayOnePeriods} chartData="tempData" chartOptions="tempOptions" />
							<br/>
							<Chart weatherData={this.state.days.dayOne.dayOnePeriods} chartData="rainData" chartOptions="rainOptions" />
						</AccordionDetails>
					</Accordion>
					<Accordion>
						<AccordionSummary expandIcon={<ExpandMore />} classes={{ root: classes.accordionSummaryRoot }}>
							<Typography variant="subtitle1" color="textSecondary" classes={{ root: classes.typographyWeekday }}>
								Tomorrow
							</Typography>
							<Typography variant="subtitle1">
								<WeatherIcon iconSize="smallWeatherIcon" selectedIconID={this.state.days.dayTwo.dayTwoPeriods[3].weather[0].id} />
								{this.state.days.dayTwo.maxTemp}° |&nbsp;
							</Typography>
							<Typography variant="subtitle1" color="textSecondary">
								{this.state.days.dayTwo.minTemp}°
							</Typography>
						</AccordionSummary>
						<AccordionDetails classes={{ root: classes.accordionDetailsRoot }}>
							<Chart weatherData={this.state.days.dayTwo.dayTwoPeriods} chartData="tempData" chartOptions="tempOptions" />
							<br/>
							<Chart weatherData={this.state.days.dayTwo.dayTwoPeriods} chartData="rainData" chartOptions="rainOptions" />
						</AccordionDetails>
					</Accordion>
					<Accordion>
						<AccordionSummary expandIcon={<ExpandMore />} classes={{ root: classes.accordionSummaryRoot }}>
							<Typography variant="subtitle1" color="textSecondary" classes={{ root: classes.typographyWeekday }}>
								{this._mapDayToName(Moment().add(2, 'd').weekday())}
							</Typography>
							<Typography variant="subtitle1">
								<WeatherIcon iconSize="smallWeatherIcon" selectedIconID={this.state.days.dayThree.dayThreePeriods[3].weather[0].id} />
								{this.state.days.dayThree.maxTemp}° |&nbsp;
							</Typography>
							<Typography variant="subtitle1" color="textSecondary">
								{this.state.days.dayThree.minTemp}°
							</Typography>
						</AccordionSummary>
						<AccordionDetails classes={{ root: classes.accordionDetailsRoot }}>
							<Chart weatherData={this.state.days.dayThree.dayThreePeriods} chartData="tempData" chartOptions="tempOptions" />
							<br/>
							<Chart weatherData={this.state.days.dayThree.dayThreePeriods} chartData="rainData" chartOptions="rainOptions" />
						</AccordionDetails>
					</Accordion>
					<Accordion>
						<AccordionSummary expandIcon={<ExpandMore />} classes={{ root: classes.accordionSummaryRoot }}>
							<Typography variant="subtitle1" color="textSecondary" classes={{ root: classes.typographyWeekday }}>
								{this._mapDayToName(Moment().add(3, 'd').weekday())}
							</Typography>
							<Typography variant="subtitle1">
								<WeatherIcon iconSize="smallWeatherIcon" selectedIconID={this.state.days.dayFour.dayFourPeriods[3].weather[0].id} />
								{this.state.days.dayFour.maxTemp}° |&nbsp;
							</Typography>
								<Typography variant="subtitle1" color="textSecondary">
									{this.state.days.dayFour.minTemp}°
								</Typography>
							</AccordionSummary>
							<AccordionDetails classes={{ root: classes.accordionDetailsRoot }}>
								<Chart weatherData={this.state.days.dayFour.dayFourPeriods} chartData="tempData" chartOptions="tempOptions" />
								<br/>
								<Chart weatherData={this.state.days.dayFour.dayFourPeriods} chartData="rainData" chartOptions="rainOptions" />
							</AccordionDetails>
						</Accordion>
					<Accordion>
						<AccordionSummary expandIcon={<ExpandMore />} classes={{ root: classes.accordionSummaryRoot }}>
							<Typography variant="subtitle1" color="textSecondary" classes={{ root: classes.typographyWeekday }}>
								{this._mapDayToName(Moment().add(4, 'd').weekday())}
							</Typography>
							<Typography variant="subtitle1">
								<WeatherIcon iconSize="smallWeatherIcon" selectedIconID={this.state.days.dayFive.dayFivePeriods[3].weather[0].id} />
								{this.state.days.dayFive.maxTemp}° |&nbsp;
							</Typography>
							<Typography variant="subtitle1" color="textSecondary">
								{this.state.days.dayFive.minTemp}°
							</Typography>
						</AccordionSummary>
						<AccordionDetails classes={{ root: classes.accordionDetailsRoot }}>
							<Chart weatherData={this.state.days.dayFive.dayFivePeriods} chartData="tempData" chartOptions="tempOptions" />
							<br/>
							<Chart weatherData={this.state.days.dayFive.dayFivePeriods} chartData="rainData" chartOptions="rainOptions" />
						</AccordionDetails>
					</Accordion>
				</Hidden>
				<Hidden smDown>
					<Grid
						container
						justifyContent="center"
						alignItems="center"
						spacing={2}
						className={classes.sectionDesktop}
					>
						<Grid item md={10}>
							<ButtonGroup fullWidth>
								{forecastButtons}
							</ButtonGroup>
						</Grid>
						<Grid item md={10}>
							{forecastCharts}						
						</Grid>
					</Grid>
				</Hidden>
			</div>
		);
	}

	async _getDays() {
		try {
			if (Object.keys(this.state.selectedCity).length > 0) {
				let response = await fetch('/weather/forecast', {
					method: 'POST',
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						latlng: {
							latitude: this.state.selectedCity.loc.coordinates[1],
							longitude: this.state.selectedCity.loc.coordinates[0]
						}
					})
				});
				let json = await response.json();
				let dayOnePeriods = [];
				let dayTwoPeriods = [];
				let dayThreePeriods = [];
				let dayFourPeriods = [];
				let dayFivePeriods = [];
				// sort response data into five consecutive days    
				json.list.forEach(period => {
					if (Moment(period.dt_txt).isBefore(Moment().endOf('day')) || Moment(period.dt_txt).isSame(Moment().add(1, 'd').startOf('day'))) {
						dayOnePeriods.push(period);
						if (Moment(period.dt_txt).isSame(Moment().add(1, 'd').startOf('day'))) {
							dayTwoPeriods.push(period);
						}
					} else if (Moment(period.dt_txt).isBefore(Moment().add(1, 'd').endOf('day')) || Moment(period.dt_txt).isSame(Moment().add(2, 'd').startOf('day'))) {
						dayTwoPeriods.push(period);
						if (Moment(period.dt_txt).isSame(Moment().add(2, 'd').startOf('day'))) {
							dayThreePeriods.push(period);
						}
					} else if (Moment(period.dt_txt).isBefore(Moment().add(2, 'd').endOf('day')) || Moment(period.dt_txt).isSame(Moment().add(3, 'd').startOf('day'))) {
						dayThreePeriods.push(period);
						if (Moment(period.dt_txt).isSame(Moment().add(3, 'd').startOf('day'))) {
							dayFourPeriods.push(period);
						}
					} else if (Moment(period.dt_txt).isBefore(Moment().add(3, 'd').endOf('day')) || Moment(period.dt_txt).isSame(Moment().add(4, 'd').startOf('day'))) {
						dayFourPeriods.push(period);
						if (Moment(period.dt_txt).isSame(Moment().add(4, 'd').startOf('day'))) {
							dayFivePeriods.push(period);
						}
					} else if (Moment(period.dt_txt).isBefore(Moment().add(4, 'd').endOf('day')) || Moment(period.dt_txt).isSame(Moment().add(5, 'd').startOf('day'))) {
						dayFivePeriods.push(period);
					}
				});
				const daysObject = {
					dayOne: {},
					dayTwo: {},
					dayThree: {},
					dayFour: {},
					dayFive: {}
				};
				daysObject.dayOne.dayOnePeriods = dayOnePeriods;
				daysObject.dayTwo.dayTwoPeriods = dayTwoPeriods;
				daysObject.dayThree.dayThreePeriods = dayThreePeriods;
				daysObject.dayFour.dayFourPeriods = dayFourPeriods;
				daysObject.dayFive.dayFivePeriods = dayFivePeriods;                     
				// calculate max and min temps
				Object.keys(daysObject).forEach(day => {
					let temps = [];
					Object.keys(daysObject[day][day + "Periods"]).forEach(period => {
						temps.push(daysObject[day][day + "Periods"][period].main.temp);
					});
					daysObject[day].maxTemp = (Math.round(Math.max(...temps)));
					daysObject[day].minTemp = (Math.round(Math.min(...temps)));                     
				});
				this.setState({ weatherData: json, days: daysObject, isLoading: false });
			}
		}
		catch(e) {
			this.setState({ isLoading: false, fetchError: true });
		}
	}

	_mapDayToName = day => {
		const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		return weekDays[day];
	}

	_setChartIndex = index => {
		if (this.state.chartIndex === index) {
			this.setState({ chartIndex: null });
		} else {
			this.setState({ chartIndex: index });
		}
	}
}

Forecast.propTypes = {
	classes: PropTypes.object.isRequired
};
  
export default withStyles(styles, { withTheme: true })(Forecast);
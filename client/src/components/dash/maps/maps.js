import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';
import { isObject } from 'lodash';
import LoadScreen from '../../loadScreen/loadScreen';
import { MapContainer, LayersControl, TileLayer } from 'react-leaflet';
import '../../../vendor/css/leaflet.css';

const styles = theme => ({
	mapSize: {
		height: 'calc(100vh - 114px)',
		marginTop: '-14px',
		maxWidth: '100%',
		[theme.breakpoints.up('md')]: {
			height: 'calc(100vh - 66px)',
			marginTop: '-18px'
    }
	}
});

class Maps extends React.Component {
	state = {
		isLoading: true,
		selectedCity: {}
	};
    
	static getDerivedStateFromProps(nextProps, prevState) {
		if ((nextProps.selectedCity !== prevState.selectedCity) && (isObject(nextProps.selectedCity) && Object.keys(nextProps.selectedCity).length > 0)) {
			return {
				selectedCity: nextProps.selectedCity,
				isLoading: false
			};
		}
  
		return null;
	}


	render() {
		const { classes } = this.props;
        
		return (
			this.state.isLoading ? 
			<LoadScreen /> :
			<MapContainer
				center={[this.state.selectedCity.loc.coordinates[1], this.state.selectedCity.loc.coordinates[0]]} zoom="8"
				className={classes.mapSize}
				touchZoom
			>
			<TileLayer
				attribution="<a href='https://foundation.wikimedia.org/wiki/Maps_Terms_of_Use' target='_blank' rel='noopener noreferrer'>Wikimedia maps</a>"
				url="/weather/maps/groundLayer/{z}/{x}/{y}"
			/>
				<LayersControl position="topright">
					<LayersControl.BaseLayer checked name="Precipitation">
						<TileLayer
							attribution="<a href='https://openweathermap.org/' target='_blank' rel='noopener noreferrer'>OpenWeatherMap</a>"
							url="/weather/maps/weatherLayer/precipitation_new/{z}/{x}/{y}"
						/>
					</LayersControl.BaseLayer>
					<LayersControl.BaseLayer name="Temperature">
						<TileLayer
							attribution="<a href='https://openweathermap.org/' target='_blank' rel='noopener noreferrer'>OpenWeatherMap</a>"
							url="/weather/maps/weatherLayer/temp_new/{z}/{x}/{y}"
						/>
					</LayersControl.BaseLayer>
					<LayersControl.BaseLayer name="Clouds">
						<TileLayer
							attribution="<a href='https://openweathermap.org/' target='_blank' rel='noopener noreferrer'>OpenWeatherMap</a>"
							url="/weather/maps/weatherLayer/clouds_new/{z}/{x}/{y}"
						/>
					</LayersControl.BaseLayer>
					<LayersControl.BaseLayer name="Wind speed">
						<TileLayer
							attribution="<a href='https://openweathermap.org/' target='_blank' rel='noopener noreferrer'>OpenWeatherMap</a>"
							url="/weather/maps/weatherLayer/wind_new/{z}/{x}/{y}"
						/>
					</LayersControl.BaseLayer>
				</LayersControl>
			</MapContainer>
		);
	}
}

Maps.propTypes = {
	classes: PropTypes.object.isRequired
};
  
export default withStyles(styles)(Maps);
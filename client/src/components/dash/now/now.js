import React from 'react';
import PropTypes from 'prop-types';
import {
	Grid,
	Typography,
	List,
	ListItem,
	ListItemText,
	ListItemIcon,
	withStyles,
	Avatar,
	Paper,
	Accordion,
	AccordionDetails,
	AccordionSummary,
	Hidden
} from '@material-ui/core';
import Moment from 'moment';
import 'moment-timezone';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ErrorIcon from '@material-ui/icons/Error';
import LoadScreen from '../../loadScreen/loadScreen';
import WeatherIcon from '../../weatherIcon/weatherIcon';
import { isArray } from 'lodash';

const styles = theme => ({
	weatherFontIcon: {
		fontSize: '24px',
		margin: '10px'
	},
	avatarImg: {
		width: '24px',
		height: 'auto'
	},
	listRoot: {
		paddingTop: 0,
		paddingBottom: 0
	},
	listItemRoot: {
		paddingTop: 0,
		paddingBottom: 0
	},
	listItemTextRoot: {
		textAlign: 'right',
		'@media (max-width: 599px)': {
			paddingLeft: '16px',
			paddingRight: '16px'
		},
		'@media (min-width: 600px)': {
			paddingLeft: 0,
			paddingRight: 0
		}
	},
	errorIcon: {
		fontSize: '3.5em',
		color: '#F44336'
	},
	paperWarning: {
		textAlign: 'center',
		paddingTop: 16,
		paddingBottom: 16,
		marginTop: theme.spacing(3)
	},
	typographyUnit: {
		verticalAlign: 'top'
	},
	h2: {
		lineHeight: 1.79
	},
	infoContainer: {
		display: 'none',
    [theme.breakpoints.up('md')]: {
			justifyContent: 'center',
      display: 'flex',
			flexWrap: 'wrap',
		},
		marginBottom: theme.spacing(2)
  },
	info: {
    margin: theme.spacing(1)
	},
	accordionDetailsRoot: {
		display: 'block',
		padding: 0
	},
	sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex'
    }
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  }
});

class Now extends React.Component {
	state = {
		isLoading: true,
		fetchError: false,    
		selectedCity: {},
		weatherData: {},
		time: ""
	};
    
	static getDerivedStateFromProps(nextProps, prevState) {
		if (nextProps.selectedCity !== prevState.selectedCity) {
			return {
				selectedCity: nextProps.selectedCity
			};
		}

		return null;
	}

	componentDidMount() {
		this._getWeatherData();
		this.updateTime = setInterval(() => this.setState({
			time: Moment.tz(new Date(), isArray(this.state.selectedCity.timezone) && this.state.selectedCity.timezone.length > 0 ? this.state.selectedCity.timezone[0] : Moment.tz.guess()).format('dddd, HH:mm')
		}), 1000);
	}

	componentWillUnmount() {
		clearInterval(this.updateTime);
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevState.selectedCity !== this.state.selectedCity) {
			this._getWeatherData();
		}
	}

	handleTabChange = (event, value) => {
    this.setState({ tabValue: value });
	};

	handleTabChangeIndex = index => {
    this.setState({ tabValue: index });
	};

	render(props) {
		const { classes } = this.props;

		return (
			this.state.isLoading && !this.state.fetchError ? 
			<LoadScreen /> :
			!this.state.isLoading && this.state.fetchError ?
			<Grid
					container
					justifyContent="center"
					alignItems="center"
				>
				<Grid item md={10}>
					<Paper className={classes.paperWarning} elevation={1}>
						<ErrorIcon className={classes.errorIcon} />
						<Typography variant="subtitle1" paragraph>
							An error occured while fetching the data. Please try again later.
						</Typography>
					</Paper>
				</Grid>
			</Grid> :
			<div>
				<Grid
					container
					justifyContent="center"
					alignItems="center"
				>
					<Grid item md={9} className={classes.infoContainer}>
						<div className={classes.info}>
							<Typography display="inline" color="textPrimary" variant="body2">
								<b>Wind: </b>	
							</Typography>
							<Typography display="inline" color="textPrimary" variant="body2">
								{Math.round(Number(this.state.weatherData.wind.speed)*3.6) + " km/h, "}
							</Typography>
							<Typography display="inline" color="textPrimary" variant="body2">
								{(isNaN(Math.round(Number(this.state.weatherData.wind.deg))) ? "-" : Math.round(Number(this.state.weatherData.wind.deg)) + "°")}
							</Typography>
						</div>
						<div className={classes.info}>
							<Typography display="inline" color="textPrimary" variant="body2">
								<b>Cloudiness: </b>
							</Typography>
							<Typography display="inline" color="textPrimary" variant="body2">
								{this.state.weatherData.clouds.all + " %"}								
							</Typography>
						</div>
						<div className={classes.info}>
							<Typography display="inline" color="textPrimary" variant="body2">
								<b>Humidity: </b>
							</Typography>
							<Typography display="inline" color="textPrimary" variant="body2">
								{this.state.weatherData.main.humidity + " %"}								
							</Typography>
						</div>
						<div className={classes.info}>
							<Typography display="inline" color="textPrimary" variant="body2">
								<b>Pressure: </b>
							</Typography>
							<Typography display="inline" color="textPrimary" variant="body2">
								{Math.round(Number(this.state.weatherData.main.pressure)) + " hPa"}
							</Typography>
						</div>
						<div className={classes.info}>
							<Typography display="inline" color="textPrimary" variant="body2">
								<b>Sunrise: </b>
							</Typography>
							<Typography display="inline" color="textPrimary" variant="body2">
								{Moment(new Date(this.state.weatherData.sys.sunrise * 1000)).format("HH:mm")}
							</Typography>
						</div>
						<div className={classes.info}>
							<Typography display="inline" color="textPrimary" variant="body2">
								<b>Sunset: </b>
							</Typography>
							<Typography display="inline" color="textPrimary" variant="body2">
								{Moment(new Date(this.state.weatherData.sys.sunset * 1000)).format("HH:mm")}
							</Typography>
						</div>
					</Grid>
				</Grid>
				<Grid
					container
					justifyContent="center"
					alignItems="center"
				>
					<Grid item xs={12} className={classes.sectionDesktop}>
						<Grid
							container
							justifyContent="center"
							alignItems="center"
						>
							<Grid item md={5}>
								<Typography align="right">
									<WeatherIcon iconSize="largeWeatherIcon" selectedIconID={this.state.weatherData.weather[0].id} iconType="now" weatherData={this.state.weatherData} />
								</Typography>
							</Grid>
							<Grid item md={6}>
								<div>
									<Typography variant="h5" color="textPrimary">
										{this.state.selectedCity.name}
									</Typography>
									<Typography variant="subtitle1" color="textSecondary">
										{" " + this.state.time}
									</Typography>
								</div>
								<Typography variant="h3" display="inline" color="textPrimary">
									{Math.round(this.state.weatherData.main.temp)}
								</Typography>
								<Typography variant="h5" display="inline" color="textPrimary" classes={{ root: classes.typographyUnit, h2: classes.h2 }}>
									°
								</Typography>
								<Typography variant="h5" display="inline" color="textPrimary" classes={{ root: classes.typographyUnit, h2: classes.h2 }}>
									C
								</Typography>
								<Typography variant="h4" display="inline" color="textSecondary">
									{" " + String(this.state.weatherData.weather[0].description).charAt(0).toUpperCase() + String(this.state.weatherData.weather[0].description).slice(1)}
								</Typography>
							</Grid>
						</Grid>
					</Grid>
					<Grid item xs={12} className={classes.sectionMobile}>
						<Grid
							container
							justifyContent="center"
							alignItems="center"
						>
							<Grid item xs={7}>
								<Typography align="right">
									<WeatherIcon iconSize="largeWeatherIcon" selectedIconID={this.state.weatherData.weather[0].id} iconType="now" weatherData={this.state.weatherData} />
								</Typography>
							</Grid>
							<Grid item xs={5}>
								<Typography variant="h3" display="inline" color="textPrimary">
									{Math.round(this.state.weatherData.main.temp)}
								</Typography>
								<Typography variant="h5" display="inline" color="textPrimary" classes={{ root: classes.typographyUnit, h2: classes.h2 }}>
									°
								</Typography>
								<Typography variant="h5" display="inline" color="textPrimary" classes={{ root: classes.typographyUnit, h2: classes.h2 }}>
									C
								</Typography>
							</Grid>
							<Grid item xs={12} align="center">
								<Typography variant="h4" display="inline" color="textSecondary">
									{" " + String(this.state.weatherData.weather[0].description).charAt(0).toUpperCase() + String(this.state.weatherData.weather[0].description).slice(1)}
								</Typography>
							</Grid>
							<Grid item xs={12} align="center">
								<br/>
								<Typography variant="h5" color="textPrimary">
									{this.state.selectedCity.name}
								</Typography>
								<Typography variant="subtitle1" color="textSecondary">
									{" " + this.state.time}
								</Typography>
							</Grid>
						</Grid>
					</Grid>
				</Grid>
				<br/>
				<Hidden mdUp>
					<Accordion>
						<AccordionSummary expandIcon={<ExpandMoreIcon />}>
							<Typography variant="subtitle1" color="textSecondary">FULL REPORT</Typography>
						</AccordionSummary>
						<AccordionDetails classes={{ root: classes.accordionDetailsRoot }}>
							<List>
								<ListItem classes={{ root: classes.listItemRoot}}>
									<Avatar classes={{ img: classes.avatarImg }} src={process.env.PUBLIC_URL + "/weather-icons/wi-strong-wind.png"} />
									<ListItemText classes={{ root: classes.listItemTextRoot }}>
										Wind: <b>{Math.round(Number(this.state.weatherData.wind.speed)*3.6)} km/h, {isNaN(Math.round(Number(this.state.weatherData.wind.deg))) ? "-" : Math.round(Number(this.state.weatherData.wind.deg))}°</b>
									</ListItemText>
								</ListItem>
								<ListItem classes={{ root: classes.listItemRoot}}>
									<Avatar classes={{ img: classes.avatarImg }} src={process.env.PUBLIC_URL + "/weather-icons/wi-cloud.png"} />
									<ListItemText classes={{ root: classes.listItemTextRoot }}>
										Cloudiness: <b>{this.state.weatherData.clouds.all} %</b>
									</ListItemText>
								</ListItem>
								<ListItem classes={{ root: classes.listItemRoot}}>
									<Avatar classes={{ img: classes.avatarImg }} src={process.env.PUBLIC_URL + "/weather-icons/wi-humidity.png"} />
									<ListItemText classes={{ root: classes.listItemTextRoot }}>
										Humidity: <b>{this.state.weatherData.main.humidity} %</b>
									</ListItemText>
								</ListItem>
								<ListItem classes={{ root: classes.listItemRoot}}>
									<ListItemIcon>
										<i className={"wi wi-barometer " + classes.weatherFontIcon}></i>
									</ListItemIcon>
									<ListItemText classes={{ root: classes.listItemTextRoot }}>
										Pressure: <b>{Math.round(Number(this.state.weatherData.main.pressure))} hPa</b>
									</ListItemText>
								</ListItem>
								<ListItem classes={{ root: classes.listItemRoot}}>
									<Avatar classes={{ img: classes.avatarImg }} src={process.env.PUBLIC_URL + "/weather-icons/wi-sunrise.png"} />
									<ListItemText classes={{ root: classes.listItemTextRoot }}>
										Sunrise: <b>{Moment(new Date(this.state.weatherData.sys.sunrise * 1000)).format("HH:mm")}</b>
									</ListItemText>
								</ListItem>
								<ListItem classes={{ root: classes.listItemRoot}}>
									<Avatar classes={{ img: classes.avatarImg }} src={process.env.PUBLIC_URL + "/weather-icons/wi-sunset.png"} />
									<ListItemText classes={{ root: classes.listItemTextRoot }}>
										Sunset: <b>{Moment(new Date(this.state.weatherData.sys.sunset * 1000)).format("HH:mm")}</b>
									</ListItemText>
								</ListItem>
							</List>
						</AccordionDetails>
					</Accordion>
				</Hidden>
			</div>
		);
	}
    
	async _getWeatherData() {
		try {
			if (Object.keys(this.state.selectedCity).length > 0) {
				let response = await fetch('/weather/now', {
					method: 'POST',
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						latlng: {
							latitude: this.state.selectedCity.loc.coordinates[1],
							longitude: this.state.selectedCity.loc.coordinates[0]
						}
					})
				});
				let json = await response.json();
				this.setState({ weatherData: json, isLoading: false });
			}
		}
		catch(e) {
			this.setState({ isLoading: false, fetchError: true });
		}
	}
}

Now.propTypes = {
    classes: PropTypes.object.isRequired
};
  
export default withStyles(styles)(Now);
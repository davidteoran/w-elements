import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
    progressParent: {
        minHeight: 400,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    progress: {
      margin: theme.spacing(2),
      color: '#757575'
    }
});

class LoadScreen extends React.Component {
    render(props) {
        const { classes } = this.props;

    	return (
            <div className={classes.progressParent}>
                <CircularProgress className={classes.progress} size={50} />
            </div>
    	)
  	}
}

LoadScreen.propTypes = {
    classes: PropTypes.object.isRequired
};
  
export default withStyles(styles)(LoadScreen);
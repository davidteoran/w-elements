import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';
import Moment from 'moment';

const styles = {
	largeWeatherIcon: {
		'@media (max-width: 599px)': {
			width: '96px',
			marginRight: '16px'
		},
		'@media (min-width: 600px)': {
			width: '124px',
			marginTop: '-16px',
			marginBottom: '-20px',
			marginRight: '16px'
		}
	},
	mediumWeatherIcon: {
		width: '32px'
	},
	smallWeatherIcon: {
		width: '20px',
		marginRight: '6px',
		marginBottom: '-4px'
	}
};

class WeatherIcon extends React.Component {
	state = {
		foundIcon: "",
		selectedIconID: ""
	};

	static getDerivedStateFromProps(nextProps, prevState) {
		if (nextProps.selectedIconID !== prevState.selectedIconID) {
			return {
				selectedIconID: nextProps.selectedIconID
			};
		}
  
		return null;
	}

	componentDidMount() {
		this._searchIconWithCode();
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevState.selectedIconID !== this.state.selectedIconID) {
			this._searchIconWithCode();
		}
	}

	render(props) {
		const { classes } = this.props;

		return (
			<img className={classes[this.props.iconSize]} src={process.env.PUBLIC_URL + "/weather-icons/" + this.state.foundIcon + ".png"} alt="" />
		)
	}

	_searchIconWithCode = () => {
		// thunderstorm with light rain or drizzle
		if (this.state.selectedIconID === 200 || this.state.selectedIconID === 230) {
			if (this.props.iconType === "now") {
				if (Moment(this.props.weatherData.sys.sunset*1000) > Moment()) {
					this.setState({ foundIcon: "wi-day-storm-showers" });
				} else {
					this.setState({ foundIcon: "wi-night-storm-showers" });
				}
			} else {
				this.setState({ foundIcon: "wi-storm-showers" });
			}
		// thunderstorm with rain or drizzle
		} else if (this.state.selectedIconID === 201 || this.state.selectedIconID === 231) {
			if (this.props.iconType === "now") {
				if (Moment(this.props.weatherData.sys.sunset*1000) > Moment()) {
					this.setState({ foundIcon: "wi-day-sleet-storm" });
				} else {
					this.setState({ foundIcon: "wi-night-sleet-storm" });
				}
			} else {
				this.setState({ foundIcon: "wi-day-sleet-storm" });
			}
		// thunderstorm with heavy rain or with heavy drizzle
		} else if (this.state.selectedIconID === 202 || this.state.selectedIconID === 232) {
			if (this.props.iconType === "now") {
				if (Moment(this.props.weatherData.sys.sunset*1000) > Moment()) {
					this.setState({ foundIcon: "wi-day-thunderstorm" });
				} else {
					this.setState({ foundIcon: "wi-night-alt-thunderstorm" });
				}
			} else {
				this.setState({ foundIcon: "wi-thunderstorm" });
			}
		// light / heavy / ragged thunderstorm
		} else if (this.state.selectedIconID === 210 || this.state.selectedIconID === 211 || this.state.selectedIconID === 212 || this.state.selectedIconID === 221) {
			this.setState({ foundIcon: "wi-lightning" });
		// light drizzle or light rain or light shower rain
		} else if (this.state.selectedIconID === 300 || this.state.selectedIconID === 500 || this.state.selectedIconID === 520) {
			if (this.props.iconType === "now") {
				if (Moment(this.props.weatherData.sys.sunset*1000) > Moment()) {
					this.setState({ foundIcon: "wi-day-showers" });
				} else {
					this.setState({ foundIcon: "wi-night-alt-showers" });
				}
			} else {
				this.setState({ foundIcon: "wi-showers" });
			}
		// drizzle or moderate rain or shower rain
		} else if(this.state.selectedIconID === 301 || this.state.selectedIconID === 501 || this.state.selectedIconID === 521) {
			if (this.props.iconType === "now") {
				if (Moment(this.props.weatherData.sys.sunset*1000) > Moment()) {
					this.setState({ foundIcon: "wi-day-sleet" });
				} else {
					this.setState({ foundIcon: "wi-night-alt-sleet" });
				}
			} else {
				this.setState({ foundIcon: "wi-sleet" });
			}
		// heavy drizzle or heavy / extreme rain or freezing rain or heavy / ragged shower rain
		} else if(this.state.selectedIconID === 302 || this.state.selectedIconID === 502 || this.state.selectedIconID === 503 || this.state.selectedIconID === 504 || this.state.selectedIconID === 511 || this.state.selectedIconID === 522 || this.state.selectedIconID === 531) {
			if (this.props.iconType === "now") {
				if (Moment(this.props.weatherData.sys.sunset*1000) > Moment()) {
					this.setState({ foundIcon: "wi-day-rain" });
				} else {
					this.setState({ foundIcon: "wi-night-alt-rain" });
				}
			} else {
				this.setState({ foundIcon: "wi-rain" });
			}
		// light / heavy drizzle rain or light / heavy rain and drizzle or rain and snow
		} else if(this.state.selectedIconID === 310 || this.state.selectedIconID === 311 || this.state.selectedIconID === 312 || this.state.selectedIconID === 313 || this.state.selectedIconID === 314 || this.state.selectedIconID === 321 || this.state.selectedIconID === 615 || this.state.selectedIconID === 616 || this.state.selectedIconID === 620 || this.state.selectedIconID === 621 || this.state.selectedIconID === 622) {
			if (this.props.iconType === "now") {
				if (Moment(this.props.weatherData.sys.sunset*1000) > Moment()) {
					this.setState({ foundIcon: "wi-day-rain-mix" });
				} else {
					this.setState({ foundIcon: "wi-night-alt-rain-mix" });
				}
			} else {
				this.setState({ foundIcon: "wi-rain-mix" });
			}
		// light / heavy snow
		} else if(this.state.selectedIconID === 600 || this.state.selectedIconID === 601 || this.state.selectedIconID === 602) {
			if (this.props.iconType === "now") {
				if (Moment(this.props.weatherData.sys.sunset*1000) > Moment()) {
					this.setState({ foundIcon: "wi-day-snow" });
				} else {
					this.setState({ foundIcon: "wi-night-alt-snow" });
				}
			} else {
				this.setState({ foundIcon: "wi-snow" });
			}
		// sleet or shower sleet
		} else if (this.state.selectedIconID === 611 || this.state.selectedIconID === 612) {
			if (this.props.iconType === "now") {
				if (Moment(this.props.weatherData.sys.sunset*1000) > Moment()) {
					this.setState({ foundIcon: "wi-day-sleet" });
				} else {
					this.setState({ foundIcon: "wi-night-alt-sleet" });
				}
			} else {
				this.setState({ foundIcon: "wi-sleet" });
			}
		// mist or fog
		} else if(this.state.selectedIconID === 700 || this.state.selectedIconID === 701 || this.state.selectedIconID === 741) {
			this.setState({ foundIcon: "wi-fog" });
		// smoke
		} else if (this.state.selectedIconID === 711) {
			this.setState({ foundIcon: "wi-smoke" });
		// haze
		} else if(this.state.selectedIconID === 721) {
			this.setState({ foundIcon: "wi-day-haze" });
		// sand / dust whirls
		} else if(this.state.selectedIconID === 731) {
			this.setState({ foundIcon: "wi-sandstorm" });
		// sand / dust
		} else if(this.state.selectedIconID === 751 || this.state.selectedIconID === 761) {
			this.setState({ foundIcon: "wi-dust" });
		// volcanic ash
		} else if (this.state.selectedIconID === 762) {
			this.setState({ foundIcon: "wi-volcano" });
		// squalls: a sudden violent gust of wind or localized storm, especially one bringing rain, snow, or sleet
		} else if (this.state.selectedIconID === 771) {
			if (this.props.iconType === "now") {
				if (Moment(this.props.weatherData.sys.sunset) > Moment()) {
					this.setState({ foundIcon: "wi-day-rain-wind" });
				} else {
					this.setState({ foundIcon: "wi-night-alt-rain-wind" });
				}
			} else {
				this.setState({ foundIcon: "wi-rain-wind" });
			}
		// tornado
		} else if(this.state.selectedIconID === 781 || this.state.selectedIconID === 900) {
				this.setState({ foundIcon: "wi-tornado" });
		// clear sky
		} else if (this.state.selectedIconID === 800) {
			if (this.props.iconType === "now") {
				if (Moment(this.props.weatherData.sys.sunset*1000) > Moment()) {
					this.setState({ foundIcon: "wi-day-sunny" });
				} else {
					this.setState({ foundIcon: "wi-night-clear" });
				}
			} else {
				this.setState({ foundIcon: "wi-day-sunny" });
			}
		// few clouds
		} else if(this.state.selectedIconID === 801) {
			if (this.props.iconType === "now") {
				if (Moment(this.props.weatherData.sys.sunset*1000) > Moment()) {
					this.setState({ foundIcon: "wi-day-cloudy" });            
				} else {
					this.setState({ foundIcon: "wi-night-alt-cloudy" });
				}
			} else {
				this.setState({ foundIcon: "wi-day-cloudy" });            
			}
		// scattered clouds
		} else if (this.state.selectedIconID === 802) {
			this.setState({ foundIcon: "wi-cloud" });
		// broken clouds
		} else if (this.state.selectedIconID === 803) {
			this.setState({ foundIcon: "wi-cloudy" });
		// overcast clouds
		} else if (this.state.selectedIconID === 804) {
			this.setState({ foundIcon: "wi-day-sunny-overcast" });            
		// tropical storm
		} else if (this.state.selectedIconID === 901) {
			this.setState({ foundIcon: "wi-rain-wind" });
		// hurricane
		} else if (this.state.selectedIconID === 902 || this.state.selectedIconID === 962) {
			this.setState({ foundIcon: "wi-hurricane" });
		// cold
		} else if (this.state.selectedIconID === 903) {
			this.setState({ foundIcon: "wi-snowflake-cold" });
		// hot
		} else if (this.state.selectedIconID === 904) {
			this.setState({ foundIcon: "wi-hot" });
		// windy
		} else if(this.state.selectedIconID === 905) {
			this.setState({ foundIcon: "wi-strong-wind" });
		// hail
		} else if (this.state.selectedIconID === 906) {
			this.setState({ foundIcon: "wi-hail" });
		// calm / light, gentle breeze / fresh-breeze
		} else if (this.state.selectedIconID === 951 || this.state.selectedIconID === 952 || this.state.selectedIconID === 953 || this.state.selectedIconID === 954 || this.state.selectedIconID === 955) {
			this.setState({ foundIcon: "wi-windy" });
		// strong breeze or high wind or storm
		} else if (this.state.selectedIconID === 956 || this.state.selectedIconID === 957 || this.state.selectedIconID === 958 || this.state.selectedIconID === 959 || this.state.selectedIconID === 960 || this.state.selectedIconID === 961) {
			this.setState({ foundIcon: "wi-strong-wind" });
		} else {
			this.setState({ foundIcon: "" });
		}
	}
}
  
WeatherIcon.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(WeatherIcon);
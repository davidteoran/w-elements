import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { MuiThemeProvider, createTheme } from '@material-ui/core/styles';
import './vendor/css/weather-icons.min.css';
import './vendor/css/weather-icons-wind.min.css';
import 'typeface-roboto';
import Welements from './App.js';
import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import reportWebVitals from './reportWebVitals';

const theme = createTheme({
	palette: {
		type: 'dark',
		primary: {
			main: '#576F72'
		},
		secondary: {
			main: '#E4DCCF'
		},
		background: {
			default: '#292836',
			paper: '#576F72'
		}
	}
});

ReactDOM.render((
	<BrowserRouter>
		<MuiThemeProvider theme={theme}>
			<Welements />
		</MuiThemeProvider>
	</BrowserRouter>
), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.register();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
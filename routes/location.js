const { find } = require('geo-tz');
const cities = require('all-the-cities');

exports.locationTz = (req, res) => {
    const locationTimezone = find(req.body.latlng.latitude, req.body.latlng.longitude);

    if (Array.isArray(locationTimezone) && locationTimezone.length > 0) {
      res.json(JSON.stringify(locationTimezone));
    } else {
      res.json(JSON.stringify([]));
    }
};

exports.location = (req, res) => {
  const filteredCities = cities.filter(city => city.name.toLowerCase().includes(req.body.cityName.toLowerCase()));

  if (Array.isArray(filteredCities) && filteredCities.length > 0) {
    res.json(JSON.stringify(filteredCities));
  } else {
    res.json(JSON.stringify([]));
  }
};
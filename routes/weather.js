const axios = require('axios').default;

exports.now = (req, res) => {
    axios.get('https://api.openweathermap.org/data/2.5/weather?units=metric&lat=' + req.body.latlng.latitude + '&lon=' + req.body.latlng.longitude + '&APPID=' + process.env.WEATHER_API_KEY)
        .then(response => {
            res.json(response.data);
        }).catch(error => {
            console.log('error:', error); // Print the error if one occurred
            res.json(JSON.stringify(error));
        });
};

exports.forecast = (req, res) => {
    axios.get('https://api.openweathermap.org/data/2.5/forecast?units=metric&lat=' + req.body.latlng.latitude + '&lon=' + req.body.latlng.longitude + '&APPID=' + process.env.WEATHER_API_KEY)
        .then(response => {
            res.json(response.data);
        }).catch(error => {
            console.log('error:', error); // Print the error if one occurred
            res.json(JSON.stringify(error));
        });
};

exports.weather = (req, res) => {
    axios.get('https://api.openweathermap.org/data/2.5/onecall?units=metric&lat=' + req.body.latlng.latitude + '&lon=' + req.body.latlng.longitude + '&exclude=' + 'minutely,hourly' + '&appid=' + process.env.WEATHER_API_KEY )
        .then(response => {
            res.json(response.data);
        }).catch(error => {
            console.log('error:', error); // Print the error if one occurred
            res.json(JSON.stringify(error));
        });
};

exports.uvIndex = (req, res) => {
    const options = {
        method: 'GET',
        url: 'https://api.openuv.io/api/v1/uv',
        params: {
            lat: req.body.latlng.latitude,
            lng: req.body.latlng.longitude 
        },
        headers: {
            'content-type': 'application/json',
            'x-access-token': process.env.UV_API_KEY
        }
    };
    axios.request(options)
        .then(response => {
            res.json(response.data);
        })
        .catch(error => {
            console.log('error:', error); // Print the error if one occurred
            res.json(JSON.stringify(error));
        });
};

exports.groundLayer = (req, res) => {
    const options = {
        method: 'GET',
        url: 'https://maps.wikimedia.org/osm-intl/'+ req.params.z +'/'+ req.params.x +'/'+ req.params.y +'.png',
        responseType: 'stream'
    };
    axios(options)
    .then(response => {
        response.data.pipe(res);
    })
    .catch(error => {
        console.log('error:', error); // Print the error if one occurred
        res.json(JSON.stringify(error));
    });
};

exports.weatherLayer = (req, res) => {
    const options = {
        method: 'GET',
        url: 'https://tile.openweathermap.org/map/'+ req.params.layer +'/'+ req.params.z +'/'+ req.params.x +'/'+ req.params.y +'.png?appid=' + process.env.WEATHER_API_KEY,
        responseType: 'stream'
    };
    axios(options)
        .then(response => {
            response.data.pipe(res);
        })
        .catch(error => {
            console.log('error:', error); // Print the error if one occurred
            res.json(JSON.stringify(error));
        });
};

const express = require('express');
const http = require('http');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const methodOverride = require('method-override');
const errorHandler = require('errorhandler');
const compression = require('compression');
const helmet = require('helmet');

const locationRoutes = require('./routes/location');
const weatherRoutes = require('./routes/weather');

const app = express();

// enable helmet security
app.use(helmet({
	contentSecurityPolicy: false
}));

// enable compression
app.use(compression());

// all environments
app.set('port', process.env.PORT || 3001);

app.use(favicon(path.join(__dirname, 'client/public/icons/favicon.ico')));
app.use(logger('dev'));
app.use(methodOverride());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Serve static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

// Location API routes
app.post('/location/timezone', locationRoutes.locationTz);
app.post('/location/cities', locationRoutes.location);
// Weather API routes
app.post('/weather/now', weatherRoutes.now);
app.post('/weather/forecast', weatherRoutes.forecast);
//app.post('/weather/weatherData', weatherRoutes.weather);
app.post('/weather/uv-index', weatherRoutes.uvIndex);
app.get('/weather/maps/groundLayer/:z/:x/:y', weatherRoutes.groundLayer);
app.get('/weather/maps/weatherLayer/:layer/:z/:x/:y', weatherRoutes.weatherLayer);

// Service worker handler
app.get('/service-worker.js', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client/build/service-worker.js'));
});

// The "catchall" handler: for any request that doesn't
// match one above, send back React's index.html file.
app.get('*', (req, res) => {
	res.sendFile(path.resolve(__dirname, 'client/build/index.html'));
});

// error handling middleware should be loaded after the loading the routes
if (app.get('env') === 'development') {
	app.use(errorHandler());
    require("dotenv").config();	
}

const server = http.createServer(app);
server.listen(app.get('port'), () => {
	console.log('Express server listening on port ' + app.get('port'))
});

// ping to prevent server sleeping
setInterval(() => {
    http.get('http://w-elements.onrender.com/dash/weather', () => {
		console.log('Application pinged to prevent sleeping!');
	});
}, 900000);

